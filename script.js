(function() {

  var DOM = {
    prefaceButton: $('#preface'),
    aboutGameButton: $('#aboutGame'),
    newGameButton: $('#newGame'),
    loadGameButton: $('#loadGame'),
    clearGameButton: $('#clear'),
    textBox: $('#text-box'),
    firstParagraphBox: $('#next-box'),
    mainMenu: $('#buttons-container'),
    menuButton: $('.menu-button'),
    sideMenu: $('.side-menu'),
    returnToMenu: $('.return-to-menu-button'),
    returnToMenuButton: $('<button/>', { id: "return", value: "return" }).text("Powrót do menu")
  };

  var JSON;
  $.getJSON('paragraph.json').done(function (data) {
    JSON = data;
    DOM.prefaceButton.on('click', preface);
    DOM.aboutGameButton.on('click', aboutGame);
    DOM.newGameButton.on('click', newGame);
    DOM.menuButton.on('click', showMenu);
    DOM.loadGameButton.on('click', loadGatheredInformation);
    DOM.clearGameButton.on('click', clearGame);
    DOM.firstParagraphBox.on('click', 'div', loadNextParagraph);
    DOM.returnToMenuButton.on('click', returnToMenu);
    DOM.returnToMenu.on('click', returnToMenu);
  });

  /* Funkcje */

  /* Funkcje wspólne dla wszystkich części gry */

  function showMenu() {
    DOM.sideMenu.animate({opacity: "toggle"});
  }

  function hideButtons(container) {
    container.hide();
  }

  function showButtons(container) {
    container.show();
  }

  function clearScreen() {
    DOM.textBox.empty();
    DOM.firstParagraphBox.empty();
  }

  function returnToMenu() {
    clearScreen();
    $('.side-menu p').empty();
    showButtons(DOM.mainMenu);
  }

  function clearGame() {
    window.localStorage.clear();
    location.reload();
    return false;
  }

  /* Funkcja wstępu */

  function preface() {
    hideButtons(DOM.mainMenu);
    var prefaceText = JSON.preface.join("<p>");
    DOM.textBox.html("<p>" + prefaceText + "</p>");
    /*    DOM.firstParagraphBox.append(DOM.returnToMenuButton);*/
  }

  function aboutGame() {
    hideButtons(DOM.mainMenu);
    var whatIsThisGameText = JSON.whatIsThisGame.join("<p>");
    DOM.textBox.html("<p>" + whatIsThisGameText + "</p>");
    /*    DOM.firstParagraphBox.append(DOM.returnToMenuButton);*/
  }

  /* Funkcje rozpoczęcia nowej gry */

  function setStartingInformation(numberOfInformation, startingParagraphID) {
    newGameInformation(numberOfInformation);
    localStorage.setItem('paragraphID', startingParagraphID);
  }

  function loadGatheredInformation() {
    for (i = 1; i <= JSON.gameData.numberOfInformations; i++ ) {
      if (localStorage.getItem('informationStatus'+i) === "yes") {
        console.log('informationStatus'+i, "yes");
        var informationContainer = $('#informationStatus'+i);
        informationContainer.text('Informacja #'+i);
      }
    }
    playGame();
  }

  function newGame() {
    var numberOfInformation = JSON.gameData.numberOfInformations;
    var startingParagraphID = JSON.gameData.startingParagraphID;
    setStartingInformation(numberOfInformation, startingParagraphID);
    playGame();
  }

  /* Funkcje głównej gry */

  function playGame() {
    hideButtons(DOM.mainMenu);
    clearScreen();
    var paragraphID = localStorage.getItem('paragraphID');
    if (paragraphID === null) {
      paragraphID = 0;
    }
    var JSONparagraph = JSON.paragraphs[paragraphID];
    gameText(JSONparagraph);
    loadNextParagraphsOptions(JSONparagraph, paragraphID);
    informationGain(paragraphID);
    localStorage.setItem('paragraphID', paragraphID);
  } 

  function gameText(JSONparagraph) {
    var paragraphText = JSONparagraph.text;
    var paragraphLength = JSONparagraph.text.length;
    for (i=0; i < paragraphLength; i++) {
      var text = paragraphText.join("<p>");
      DOM.textBox.html("<p>" + text + "</p>");
    }
  }

  function loadNextParagraphsOptions(JSONparagraph, paragraphID) {
    var actionLength = JSONparagraph.actions.length;
    for (i=0; i < actionLength; i++) {
      var clickingText = JSONparagraph.actions[i].clickingText;
      var nextParagraphID = JSONparagraph.actions[i].nextParagraphID;
      var hiddenText = JSONparagraph.actions[i].hiddenText;
      var requiredInformationID = JSONparagraph.actions[i].requiredInformationID;
      var informationStatus = localStorage.getItem('informationStatus'+requiredInformationID);
      if (JSONparagraph.actions[i].requiredInformationID !== 0 && informationStatus === "yes") {
        checkHiddenParagraphs(hiddenText, informationStatus, nextParagraphID, clickingText);
        if (JSONparagraph.actions[i].shownAlone === "yes") {
          break;
        }
      }
      else if (JSONparagraph.actions[i].requiredInformationID !== 0 && informationStatus === "no") {
        continue;
      }
      else {
        checkHiddenParagraphs(hiddenText, informationStatus, nextParagraphID, clickingText);
      } 
      isLastParagraph(paragraphID, JSONparagraph);
    }
  }

/*  function showDefaultParagraph(hiddenText, nextParagraphID, clickingText) {
    if (hiddenText !== "") {
      var hiddenParagraphText = hiddenText.join("<p>");
      DOM.textBox.html("<p>" + hiddenParagraphText + "</p>");
    } else {
    }
    var nextParagraphText = $("<p>").attr({"ID": i, "pageNumber": nextParagraphID}).append(clickingText);
    nextParagraphText.appendTo(DOM.firstParagraphBox);
  }*/

  function checkHiddenParagraphs(hiddenText, informationStatus, nextParagraphID, clickingText) {
    if (hiddenText === "") {
    } 
    else {    
      var hiddenParagraphText = hiddenText.join("<p>");
      DOM.textBox.html("<p>" + hiddenParagraphText + "</p>");
    }
    var nextParagraphText = $("<div>").attr({"ID": i, "pageNumber": nextParagraphID, "class": "nextParagraph"}).append(clickingText);
    nextParagraphText.appendTo(DOM.firstParagraphBox);
  }

  function newGameInformation(informationNumber) {
    localStorage.setItem('informationStatus'+0, "yes");
    console.log('informationStatus'+0, "yes");
    for (i = 1; i <= informationNumber; i++) {
      localStorage.setItem('informationStatus'+i, "no");
      console.log('informationStatus'+i, "no");
    }
  }

  function informationGain(paragraphID) {
    var actions = JSON.paragraphs[paragraphID].actions;
    var actionsLength = actions.length;
    for (i = 0; i < actionsLength; i++ ) {
      if (actions[i].acquiredInformationID !== 0) {
        var acquiredInformationID = actions[i].acquiredInformationID;
        localStorage.setItem('informationStatus'+acquiredInformationID, "yes");
        console.log('informationStatus'+acquiredInformationID, "yes");
        if ($('#informationStatus'+acquiredInformationID).text().length <= 0) {
          $('#informationStatus'+acquiredInformationID).text('Informacja #'+acquiredInformationID);
        }
      }
    }
  }

  function isLastParagraph(paragraphID, JSONparagraph) {
    var isLastParagraph = JSONparagraph.actions[0].isLastParagraph;
    if (isLastParagraph == "yes") {
      DOM.firstParagraphBox.hide().delay(1000).append(DOM.returnToMenuButton).fadeIn(1000);
    }
  }

  function loadNextParagraph() {
    $('html,body').scrollTop(0);
    var pagenumber = $(this).attr('pagenumber');
    localStorage.setItem('paragraphID', pagenumber);
    playGame();
  }

})();